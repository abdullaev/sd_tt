VIN Decoder API (test task)
=============================

REQUIREMENTS
------------

The minimum requirement for Python version 3

You should also have: 

    Django >= 2.0
    djangorestframework >= 3.8
    psycopg2 >= 2.7
    requests >= 2.1

For more information see `requirements.txt`


INSTALLATION
------------
**Using Docker**

    docker-compose up
Once the services are running, you need to perform the database migration

    docker-compose run web /usr/local/bin/python manage.py migrate

**Classic way**

Install all packages

    pip install -r requirements.txt

Update database settings in `/sd_tt/settings.py`

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql',
            'NAME': 'DATABASE_NAME',
            'USER': 'USERNAME',
            'PASSWORD': 'PASSWORD',
            'HOST': 'localhost',
            'PORT': '',
        }
    }
    
Create all needed tables

    python manage.py migrate

Run the project

    python manage.py runserver 8000

For more information see `swagger.yaml` 
