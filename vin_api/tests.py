from django.test import TestCase, Client
from django.urls import reverse
import json

from .helpers import find_or_get_data

TEST_VALID_VIN = '1C4RJEBM5EC581437'
TEST_WRONG_VIN = '1234567890ABCDEFG'


class HelpersTests(TestCase):
    def test_find_or_get_data_with_valid_vin(self):
        data = find_or_get_data(TEST_VALID_VIN)
        data_length = len(data)
        # self.assertGreater(data_length, 3)
        '''
        In positive case Json-data length must be equal 9
        '''
        self.assertEqual(data_length, 9)


class ApiTests(TestCase):
    def test_without_params(self):
        client = Client()
        response = client.get(reverse('api:index'))
        self.assertEqual(response.status_code, 500)

    def test_with_wrong_method(self):
        client = Client()
        response = client.post(reverse('api:index'), {'vin': TEST_VALID_VIN})
        content = json.loads(response.content)
        self.assertEqual(content['detail'], 'Method "POST" not allowed.')

    def test_with_wrong_parameter(self):
        client = Client()
        response = client.get(reverse('api:index'), {'vin': TEST_WRONG_VIN})
        content = json.loads(response.content)
        self.assertEqual(content['type'], 'API error')
