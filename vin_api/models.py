from django.db import models
import json


class Vin(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    # data from api
    year = models.IntegerField()
    make = models.CharField(max_length=200)
    model = models.CharField(max_length=200)
    type = models.CharField(max_length=200, blank=True, default='')
    color = models.CharField(max_length=200, blank=True, default='')
    dimensions = models.CharField(max_length=200, blank=True, default='')
    weight = models.CharField(max_length=100, blank=True, default='')
    details = models.TextField(blank=True, default='')

    def as_json(self):
        return {
            'vin': self.vin,
            'year': self.year,
            'make': self.make,
            'model': self.model,
            'type': self.type,
            'color': self.color,
            'weight': self.weight,
            'dimensions': self.dimensions,
            # 'details': json.loads(self.details) if type(self.details) is str else self.details,
        }

    @classmethod
    def add_new(cls, data):
        """
        Write new record in DB
        https://docs.djangoproject.com/en/2.0/ref/models/instances/
        """
        model = cls(
            vin=data['vin'],
            year=data['year'],
            make=data['make'],
            type=data['type'],
            model=data['model'],
            color=data['color'],
            weight=data['weight'],
            dimensions=data['dimensions'],
            details=json.dumps(data['details'])
        )
        model.save()
        return model
