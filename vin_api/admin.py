from django.contrib import admin
from .models import Vin


class VinAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['vin']}),
        ('Required Information', {'fields': ['year', 'make', 'model']}),
        ('Additional Information', {'fields': ['type', 'color', 'weight', 'dimensions', 'details']}),
    ]

    list_display = ['vin', 'year', 'make', 'model']
    list_filter = ['year', 'make']
    search_fields = ['vin']


admin.site.register(Vin, VinAdmin)
