from django.apps import AppConfig


class VinAppConfig(AppConfig):
    name = 'vin_api'
