from django.http import JsonResponse
from rest_framework.decorators import api_view
from .helpers import find_or_get_data


@api_view(['GET'])
def get_data_by_vin(request):
    if 'vin' not in request.GET:
        return JsonResponse({
            'status': 'ERROR',
            'type': 'Wrong parameter',
            'message': 'VIN should be in url as a GET parameter'
        }, status=500)

    vin = request.GET['vin']
    if len(vin) != 17:
        return JsonResponse({
            'status': 'ERROR',
            'type': 'Wrong parameter',
            'message': 'VIN most consist 17 symbols'
        }, status=500)

    data = find_or_get_data(vin)
    if 'status' in data and data['status'] == 'ERROR':
        return JsonResponse(data, status=500)

    return JsonResponse({
        'status': 'SUCCESS',
        'message': 'OK',
        'data': data,
    }, status=200)
