import requests
from abc import ABC, abstractmethod

from .models import Vin


def find_or_get_data(vin):
    """
    The method search a record in DB by VIN and return it,
    in negative case try to load data from API or return error message
    """
    model = Vin.objects.filter(vin=vin).first()

    if not model:
        try:
            # Load data from API
            data = get_data_from_api(API_DecodeThis, vin)
            # Save new record in DB in positive case from API
            model = Vin.add_new(data)
        except Exception as ex:
            return {
                'status': 'ERROR',
                'type': 'API error',
                'message': ex.__str__()
            }

    return model.as_json()


def get_data_from_api(api_class, vin):
    """ The method work like a `factory` pattern """
    api = api_class()
    return api.run(vin)


class BaseApi(ABC):
    """
    Abstract class for each API
    If you need to change API you just need to write new class and rewrite `run` method
    """

    __API_URL__ = ''
    __API_KEY__ = ''

    @abstractmethod
    def run(self, vin):
        pass


class API_DecodeThis(BaseApi):
    """ The method load data from `decodethis.com` """

    __API_URL__ = 'https://www.decodethis.com/webservices/decodes/'
    __API_KEY__ = 'QXWgcnbgyqecCh4jQzRA'

    def run(self, vin):
        url = '%s%s/%s/1.json' % (self.__API_URL__, vin, self.__API_KEY__)
        request = requests.get(url)

        if request.status_code != 200:
            raise Exception('Wrong request')

        data = request.json()
        if 'decode' not in data:
            raise Exception('Invalid data')
        if 'status' not in data['decode'] or data['decode']['status'] != 'SUCCESS':
            raise Exception('Wrong answer')

        __util_data__ = self.get_util_data(data)

        return {
            'vin': data['decode']['VIN'],
            'year': self.get_data_if_exists(data, 'year'),
            'make': self.get_data_if_exists(data, 'make'),
            'type': '%s - %s' % (self.get_data_if_exists(data, 'body'), self.get_data_if_exists(data, 'engine')),
            'model': self.get_data_if_exists(data, 'model'),
            'color': __util_data__['color'] if 'color' in __util_data__ else '',
            'weight': __util_data__['weight'] if 'weight' in __util_data__ else '',
            'dimensions': __util_data__['dimensions'] if 'dimensions' in __util_data__ else '',
            'details': self.get_data_if_exists(data, 'Equip'),
        }

    def get_data_if_exists(self, data, key):
        vehicle = data['decode']['vehicle'][0]
        if key in vehicle:
            return vehicle[key]
        return ''

    def get_util_data(self, data):
        util_data = {}
        dimensions = ['' for _ in range(3)]
        for item in self.get_data_if_exists(data, 'Equip'):
            if 'name' not in item:
                continue

            if item['name'] == 'Exterior Color':
                util_data.setdefault('color', item['value'])
            elif item['name'] == 'Curb Weight-automatic':
                util_data.setdefault('weight', item['value'] + item['unit'])

            elif item['name'] == 'Overall Width':
                dimensions[0] = item['value'] + item['unit']
            elif item['name'] == 'Overall Length':
                dimensions[1] = item['value'] + item['unit']
            elif item['name'] == 'Overall Height':
                dimensions[2] = item['value'] + item['unit']

        util_data.setdefault('dimensions', ' x '.join(dimensions))

        return util_data
